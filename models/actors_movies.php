<?php

class Actors_movies {
    
    private $id;
    private $actor_id;
    private $movie_id;
    private $cantidad;
    
    public function __construct() {
        $this->db = Database::connect();
    }
    
    function getId() {
        return $this->id;
    }

    function getActor_id() {
        return $this->actor_id;
    }

    function getMovie_id() {
        return $this->movie_id;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setActor_id($actor_id) {
        $this->actor_id = $actor_id;
    }

    function setMovie_id($movie_id) {
        $this->movie_id = $movie_id;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    
    public function save() {
        $sql = "INSERT INTO actors_movies VALUES (NULL, {$this->getActor_id()}, {$this->getMovie_id()}, {$this->getCantidad()} );";
        $save = $this->db->query($sql);

        $result = false;
        if ($save) {
            $result = true;
            //var_dump($result);
            //die();
        }
        return $result;
        
    }
    
    public function listar() {
        
        // consulta para la integracion
        
      // sql = "select am.*, a.nombre, m.nombre from actors_movies am INNER JOIN actors a ON a.id = am.actor_id INNER JOIN movies m ON m.id = am.movie_id;";
    }
            
    
}