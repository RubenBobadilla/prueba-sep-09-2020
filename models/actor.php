<?php

class Actor {

    private $id;
    private $nombre;    
    private $db;

    public function __construct() {
        $this->db = Database::connect();
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $this->db->real_escape_string($nombre);
    }

    public function save() {
        $sql = "INSERT INTO actors VALUES (NULL, '{$this->getNombre()}');";
        $save = $this->db->query($sql);

        $result = false;
        if ($save) {
            $result = true;
            //var_dump($result);
            //die();
        }
        return $result;
        
    }    
    

}
