<?php

class Movie {

    private $id;
    private $nombre;    
    private $fecha;    
    private $db;

    public function __construct() {
        $this->db = Database::connect();
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }
    
    function getFecha() {
        return $this->fecha;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $this->db->real_escape_string($nombre);
    }
    
    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function save() {
        $sql = "INSERT INTO movies VALUES (NULL, '{$this->getNombre()}', CURDATE());";
        $save = $this->db->query($sql);

        $result = false;
        if ($save) {
            $result = true;
            //var_dump($result);
            //die();
        }
        return $result;
        
    }


}
