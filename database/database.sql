CREATE DATABASE actos_movies;
USE actos_movies;

CREATE TABLE actors(
id          int(255) auto_increment not null,
nombre      varchar(100) not null,  
CONSTRAINT pk_actor PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE movies(
id          int(255) auto_increment not null,
nombre      varchar(100) not null, 
fecha       date, 
CONSTRAINT pk_movie PRIMARY KEY(id)
)ENGINE=InnoDb;


CREATE TABLE actors_movies(
id          int(255) auto_increment not null,
actor_id    int(255) not null, 
movie_id    int(255) not null,
cantidad    int(255) not null,
CONSTRAINT pk_movie PRIMARY KEY(id),
CONSTRAINT fk_actors_movies_actor FOREIGN KEY(actor_id) REFERENCES actors(id),
CONSTRAINT fk_actors_movies_movie FOREIGN KEY(movie_id) REFERENCES movies(id)
)ENGINE=InnoDb;








