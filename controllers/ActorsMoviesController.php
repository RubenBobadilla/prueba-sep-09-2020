<?php

require_once 'models/actors_movies.php';

class ActorsMoviesController {

    public function index() {
        echo "";
    }

    public function create() {
        require_once 'views/actors_movie/create.php';
    }

    public function save() {
        if (isset($_POST)) {
            $Actor_m = new Actors_movies();
            $Actor_m->setActor_id($_POST['actor_id']);
            $Actor_m->setMovie_id($_POST['movie_id']);
            $Actor_m->setCantidad($_POST['cantidad']);
       //var_dump($actor);
       //die();

            $save = $Actor_m->save();
            if ($save) {
                $_SESSION['register'] = "complete";
            } else {
                $_SESSION['register'] = "failed";
            }
        } else {
            $_SESSION['register'] = "failed";
        }
        header("Location:" . base_url . 'actor/create');
    }

    public function listar() {
        $actor_m = new Actors_movies();
        $list = $actor_m->listar();
        if (!empty($list)) {

            require_once 'views/vistaa/list.php';
        } else {
            header("Location:" . base_url);
        }
    }

    

}
