<?php

require_once 'models/movie.php';

class MovieController {

    public function index() {
        echo "";
    }

    public function create() {
        require_once 'views/movie/create.php';
    }

    public function save() {
        if (isset($_POST)) {
            $movie = new Movie();
            $movie->setNombre($_POST['nombre']);
       //var_dump($actor);
       //die();

            $save = $movie->save();
            if ($save) {
                $_SESSION['register'] = "complete";
            } else {
                $_SESSION['register'] = "failed";
            }
        } else {
            $_SESSION['register'] = "failed";
        }
        header("Location:" . base_url . 'movie/create');
    }

   

}
