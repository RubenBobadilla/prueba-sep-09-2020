<?php

require_once 'models/actor.php';

class ActorController {

    public function index() {
        echo "";
    }

    public function create() {
        require_once 'views/actor/create.php';
    }

    public function save() {
        if (isset($_POST)) {
            $actor = new Actor();
            $actor->setNombre($_POST['nombre']);
       //var_dump($actor);
       //die();

            $save = $actor->save();
            if ($save) {
                $_SESSION['register'] = "complete";
            } else {
                $_SESSION['register'] = "failed";
            }
        } else {
            $_SESSION['register'] = "failed";
        }
        header("Location:" . base_url . 'actor/create');
    }

    public function listar() {
        $clienteL = new client();
        $list = $clienteL->listar();
        if (!empty($list)) {

            require_once 'views/clients/list.php';
        } else {
            header("Location:" . base_url);
        }
    }

    
}
