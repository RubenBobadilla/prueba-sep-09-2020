<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Movies</title>
        <link rel="stylesheet" href="<?=base_url?>assets/css/styles.css" />
    </head>
    <body>
        <div id="container">
            <!-- CABECERA -->
            <header id="header">
                <div id="logo">
                    <img src="assets/img/movies.jpg" alt="movies logo">
                    <a href="<?=base_url?>">
                        Opciones movies
                    </a>
                </div>
            </header>

            <nav id="menu">
            <!-- MENU -->
                <ul>
                    <li>
                        <a href="<?=base_url?>">Inicio</a>
                    </li>
                    <li>
                        <a href="#">Sobre nosotros</a>
                    </li>
                    <li>
                        <a href="#">Contacto</a>
                    </li>
                </ul>
            </nav>

            <div id="content">